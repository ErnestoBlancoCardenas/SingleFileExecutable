﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

/*
 * First approach
 */

/*
 * This Solution Works when the dll is added to the project
 * and the build action is set to Embedded Resource.
 * GetManifestResourceStream works correct with the namespace
 * of the dll.
 
   <Target Name="BeforeBuild">
    <ItemGroup>
      <EmbeddedResource Include="DLL\*.dll">
        <CopyToOutputDirectory>Never</CopyToOutputDirectory>
      </EmbeddedResource>
    </ItemGroup>
  </Target>
  <Target Name="AfterBuild">
    <Exec Command="del $(OutputPath)\*.dll"/>
  </Target>
 */
namespace DllPackerExample
{
    public class DLLLoader
    {
        static Dictionary<string, Assembly> _libs = new Dictionary<string, Assembly>();

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve +=FindAssembly;
            Program.Start(args);
        }

        static Assembly FindAssembly(object sender, ResolveEventArgs args)
        {
            string assemblyname = new AssemblyName(args.Name).Name;
            if (_libs.ContainsKey(assemblyname)) return _libs[assemblyname];
          
            using (Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("DllPackerExample.DLL." + assemblyname + ".dll"))
            {
                byte[] data = new BinaryReader(s).ReadBytes((int)s.Length);
                Assembly assembly = Assembly.Load(data);
                _libs[assemblyname] = assembly;
                return assembly;
            }
        }
    }
}

/*
 * Second approach
 */

/*
 * This Solution Works when the dll is added Resources.resx 
 * and the build action is set to Embedded Resource
 */

//namespace DllPackerExample
//{
//    public class DLLLoader
//    {
//        static Dictionary<string, Assembly> _libs = new Dictionary<string, Assembly>();

//        static void Main(string[] args)
//        {
//            AppDomain.CurrentDomain.AssemblyResolve += FindAssembly;
//            Program.Start(args);
//        }

//        public static Assembly FindAssembly(object sender, ResolveEventArgs args)
//        {
//            string name = new AssemblyName(args.Name).Name.Replace(".", "_");
//            if (_libs.ContainsKey(name)) return _libs[name];
//            if (name.EndsWith("_resources")) return null;
//            ResourceManager resourcemanager = Resources.ResourceManager;
//            byte[] bytes = (byte[])resourcemanager.GetObject(name);
//            Assembly assembly = Assembly.Load(bytes);
//            _libs[name] = assembly;
//            return assembly;
//        }
//    }
//}
